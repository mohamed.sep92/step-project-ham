### DAN.IT-Step-Project-Ham

Non-Adaptive Landing Page

### Technologies Index

1. HTML5, CSS(SCSS), JS(ES6)
2. jQuery, Slick Slider, Masonry js grid layout

### Teamwork:

[Mohamed](https://gitlab.com/mohamed.sep92/step-project-ham)

### Gitlab pages:

[visit our gitlab page](https://mohamed.sep92.gitlab.io/step-project-ham/)
